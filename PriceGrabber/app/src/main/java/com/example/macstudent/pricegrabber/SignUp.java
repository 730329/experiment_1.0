package com.example.macstudent.pricegrabber;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class SignUp extends AppCompatActivity implements View.OnClickListener{

    Button btnSignUp;
    EditText edtName;
    EditText edtPhone;
    EditText edtEmail;
    EditText edtPassword;

    DBHelper dbHelper;
    SQLiteDatabase PGDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        btnSignUp=findViewById(R.id.btnRegister);
        btnSignUp.setOnClickListener(this);

        edtName=findViewById(R.id.edtName);
        edtPhone=findViewById(R.id.edtPhone);
        edtEmail=findViewById(R.id.edtEmail);
        edtPassword=findViewById(R.id.edtPassword);

        dbHelper = new DBHelper(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==btnSignUp.getId()){
           // String data = edtName.getText().toString() +"\n"+edtPhone.getText().toString()+"\n"+
           //         edtEmail.getText().toString()+"\n"+edtPassword.getText().toString();

           // Toast.makeText(this,data,Toast.LENGTH_LONG).show();

            //  Intent loginIntent=new Intent(this,LoginActivity.class);
            //startActivity(loginIntent);

           // startActivity(new Intent(this,Login.class));

            insertData();
            displayData();
            Intent loginIntent = new Intent(this, Login.class);
            startActivity(loginIntent);
        }
    }

    private void insertData(){
        String name = edtName.getText().toString();
        String phone = edtPhone.getText().toString();
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();


        ContentValues cv = new ContentValues();
        cv.put("Name",name);
        cv.put("Phone",phone);
        cv.put("Email", email);
        cv.put("Password", password);


        try{
            PGDB = dbHelper.getWritableDatabase();
            PGDB.insert("UserInfo",null, cv);
            Log.v("SignUp", "Account created");

        }catch(Exception e){
            Log.e("SignUp",e.getMessage());
        }finally {
            PGDB.close();
        }
    }

    private void displayData(){
        try{
            PGDB = dbHelper.getReadableDatabase();
            String columns[] = {"Name","Phone","Email","Password"};

            Cursor cursor = PGDB.query("UserInfo",columns,null,
                    null,null,null,null);

            while(cursor.moveToNext())
            {
                String UserData = cursor.getString(cursor.getColumnIndex("Name"));
                UserData += "\n" + cursor.getString(cursor.getColumnIndex("Phone"));
                UserData += "\n" + cursor.getString(cursor.getColumnIndex("Email"));
                UserData += "\n" + cursor.getString(cursor.getColumnIndex("Password"));

                Toast.makeText(this, UserData, Toast.LENGTH_LONG).show();
            }

        }
        catch(Exception e)
        {
            Log.e("SignUp",e.getMessage());
        }
        finally
        {
            PGDB.close();
        }

    }

}
