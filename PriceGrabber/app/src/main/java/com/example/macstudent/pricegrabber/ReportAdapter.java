package com.example.macstudent.pricegrabber;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ReportAdapter extends BaseAdapter {

    LayoutInflater inflater;
    Context context;
    String[] amounts ={"10","20","1","5","6","7","2","13","11"};
    String[] store = {"Wallmart", "Foodbasic", "Nofrills"};

    ReportAdapter(Context context)
    {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.list_report_item, null);


        TextView txtAmount = view.findViewById(R.id.txtAmount);
        txtAmount.setText("$" + amounts[position]);

        TextView txtLot = view.findViewById(R.id.txtStore);
        txtLot.setText("Lot : " + store[position]);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Store " + position + " selected", Toast.LENGTH_LONG).show();
            }
        });
        return view;
    }
}
