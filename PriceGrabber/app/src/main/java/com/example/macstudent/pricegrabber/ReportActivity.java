package com.example.macstudent.pricegrabber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class ReportActivity extends AppCompatActivity {

    ListView lstRep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        lstRep = findViewById(R.id.lstReport);
        lstRep.setAdapter(new ReportAdapter(getApplicationContext()));
    }
}
