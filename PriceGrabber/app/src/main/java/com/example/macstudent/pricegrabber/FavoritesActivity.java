package com.example.macstudent.pricegrabber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class FavoritesActivity extends AppCompatActivity {

    TextView txtStore, txtProduct, txtPrice;

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        SharedPreferences sp = getSharedPreferences("com.example.macstudent.login.shared", Context.MODE_PRIVATE);

        txtStore = findViewById(R.id.txtStore);
        txtStore.setText(sp.getString("Store","Data Missing"));

        txtProduct = findViewById(R.id. txtProduct);
        txtProduct.setText("Lot " + sp.getString("Product", "Data Missing"));

        txtPrice = findViewById(R.id.txtPrice);
        txtPrice.setText(String.valueOf(sp.getInt("Price", 0)));

    }
}
