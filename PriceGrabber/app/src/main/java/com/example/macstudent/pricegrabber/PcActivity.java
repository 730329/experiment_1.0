package com.example.macstudent.pricegrabber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

public class PcActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {



    Spinner spnStore;
    TextView txtAmount;
    String store[] = {"Wallmart","NoFrills","FoodBasics"};
    int productRate[] = {10,20,1,5,6,7,2,13,11,9};
    String selectedStore;

    RadioButton rdo1, rdo2, rdo3, rdo4, rdo5, rdo6, rdo7, rdo8, rdo9, rdo10;
    Button btnAddFavorite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pc);

        spnStore = findViewById(R.id.spnStore);
        ArrayAdapter storeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, store);
        storeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnStore.setAdapter(storeAdapter);
        spnStore.setOnItemSelectedListener(this);

        btnAddFavorite = findViewById(R.id.btnAddFavorite);
        btnAddFavorite.setOnClickListener(this);

        rdo1 = findViewById(R.id.rdo1);
        rdo1.setOnClickListener(this);

        rdo2 = findViewById(R.id.rdo2);
        rdo2.setOnClickListener(this);

        rdo3 = findViewById(R.id.rdo3);
        rdo3.setOnClickListener(this);

        rdo4 = findViewById(R.id.rdo4);
        rdo4.setOnClickListener(this);

        rdo5 = findViewById(R.id.rdo5);
        rdo5.setOnClickListener(this);

        rdo6 = findViewById(R.id.rdo6);
        rdo6.setOnClickListener(this);

        rdo7 = findViewById(R.id.rdo7);
        rdo7.setOnClickListener(this);

        rdo8 = findViewById(R.id.rdo8);
        rdo8.setOnClickListener(this);

        rdo9 = findViewById(R.id.rdo9);
        rdo9.setOnClickListener(this);

        rdo10 = findViewById(R.id.rdo10);
        rdo10.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if(rdo1.isChecked()){
            txtAmount.setText("$" + productRate[0]);
        }else if (rdo2.isChecked()){
            txtAmount.setText("$" + productRate[1]);
        }else if (rdo3.isChecked()){
            txtAmount.setText("$" + productRate[2]);
        }else if (rdo4.isChecked()){
            txtAmount.setText("$" + productRate[3]);
        }
        else if (rdo5.isChecked()){
            txtAmount.setText("$" + productRate[4]);
        }
        else if (rdo6.isChecked()){
            txtAmount.setText("$" + productRate[5]);
        }
        else if (rdo7.isChecked()){
            txtAmount.setText("$" + productRate[6]);
        }
        else if (rdo8.isChecked()){
            txtAmount.setText("$" + productRate[7]);
        }
        else if (rdo9.isChecked()){
            txtAmount.setText("$" + productRate[8]);
        }
        else if (rdo10.isChecked()){
            txtAmount.setText("$" + productRate[9]);
        }

        if (btnAddFavorite.getId() == view.getId()){
            SharedPreferences sp = getSharedPreferences("com.example.macstudent.login.shared", Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = sp.edit();

            edit.putString("Store", selectedStore);

            edit.putInt("Amount", Integer.parseInt(txtAmount.getText().toString().substring(1)));

            edit.commit();

            startActivity(new Intent(getApplicationContext(), FavoritesActivity.class));
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        if(adapterView.getId() == spnStore.getId()){
            selectedStore = store[position];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
