package com.example.shivamgandhi.http;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class forgotActivity extends AppCompatActivity {

    EditText student_id,emergencypin;
    TextView result;
    Button go,logIn;
    String student_info,emergecyPin,password;
    boolean b=false;

    String URLDATA = "http://172.20.10.5/basic/user_info.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        student_id = findViewById(R.id.forgotpassActivity_student_id);
        emergencypin = findViewById(R.id.forgotpassActivity_emergency);
        result = findViewById(R.id.forgotpassActivity_displayResult);
        go = findViewById(R.id.forgotpassActivity_go);
        logIn = findViewById(R.id.forgotpassActivity_login);

        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 student_info = student_id.getText().toString();
                 emergecyPin = emergencypin.getText().toString();

                if(student_info.isEmpty() || emergecyPin.isEmpty()){

                    Toast.makeText(forgotActivity.this, "Fields cant be empty", Toast.LENGTH_SHORT).show();

                }
                else{

                    new forgotActivity.Getdata().execute();

                }
            }
        });

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(forgotActivity.this,loginActivity.class);
                startActivity(intent);
            }
        });

    }

    class Getdata extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd= new ProgressDialog(forgotActivity.this);
            pd.setTitle("Loading");
            pd.setMessage("Please Wait");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceClass serviceClass = new ServiceClass();
            String result = serviceClass.getData(URLDATA);
            Log.e("wait what? :: ", "" + result);

            try {
                if (result != null) {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString("student_id");
                        String price = jsonObject.getString("security_number");
                        String pass = jsonObject.getString("password");

                        if(name.equals(student_id.getText().toString()) && price.equals(emergencypin.getText().toString()))
                        {
                            Log.e("inside","inside");
                            b = true;
                            password = pass;
                        }



                    }
                }
            } catch (Exception e) {
                Log.d("URL CALLING FAILED","ERROR");
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            pd.dismiss();
            if(b) {
                forgotActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        result.setText("Password:- " + password);
                        result.setVisibility(View.VISIBLE);
                        logIn.setVisibility(View.VISIBLE);
                        go.setVisibility(View.INVISIBLE);
                    }
                });

            }
            else{
                Toast.makeText(forgotActivity.this, "Credentials are incorrect!", Toast.LENGTH_SHORT).show();
                student_id.setText("");
                emergencypin.setText("");
            }
        }
    }
}
