package com.example.shivamgandhi.http;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class loginActivity extends AppCompatActivity {

    EditText student_info;
    EditText password;
    Button logIn,signUp;
    TextView forgotPass;
    String URLDATA = "http://172.20.10.5/basic/user_info.php";
    String s_id = "student_info";
    String pass_word = "password";
    ListView listView;
    ArrayList<HashMap<String,String>> arrayList ;
    String student_id,p_word;
    boolean b = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        student_info = findViewById(R.id.loginActivity_student_id);
        password = findViewById(R.id.loginActivity_password);
        logIn = findViewById(R.id.loginActivity_log_in);
        signUp = findViewById(R.id.loginActivity_signup);
        forgotPass = findViewById(R.id.loginActivity_forgotPass);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                 student_id = student_info.getText().toString();
                 p_word = password.getText().toString();



                new Getdata().execute();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(loginActivity.this,signupActivity.class);
                startActivity(i1);
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i4 = new Intent(loginActivity.this,forgotActivity.class);
                startActivity(i4);
            }
        });
    }


        class Getdata extends AsyncTask<Void, Void, Void> {
            ProgressDialog pd;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd = new ProgressDialog(loginActivity.this);
                pd.setTitle("Loading");
                pd.setMessage("Please Wait");
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... params) {

                ServiceClass serviceClass = new ServiceClass();
                String result = serviceClass.getData(URLDATA);
                Log.e("wait what? :: ", "" + result);

                try {
                    if (result != null) {
                        Log.e("error before array", "dbh");
                        JSONArray jsonArray = new JSONArray(result);
                      //  Log.e("length of array", "" + jsonArray.length());
                        for (int i = 0; i < jsonArray.length(); i++) {

                           // Log.e("error after array", "dbh");
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            //int id = jsonObject.getInt(ID);
                            String name = jsonObject.getString("student_id");
                            String price = jsonObject.getString("password");
                            //Log.e("name:- ",name);
                            if(name.equals(student_id) && price.equals(p_word))
                            {
                                b = true;
                            }


                        }
                    }
                } catch (Exception e) {

                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                pd.dismiss();
                if(b) {
                    Intent i = new Intent(loginActivity.this, splashActivity.class);
                    i.putExtra("student_id", student_id);
                    startActivity(i);
                }
                else{
                    student_info.setText("");
                    password.setText("");
                    Toast.makeText(loginActivity.this, "Credentials are wrong! Try again.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }



