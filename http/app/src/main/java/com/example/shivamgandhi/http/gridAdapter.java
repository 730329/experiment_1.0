package com.example.shivamgandhi.http;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

public class gridAdapter extends BaseAdapter {
    private final Context mContext;
    private  final int length ;

    public gridAdapter(Context context,  int length){
        this.mContext = context;

        this.length = length;
    }

    @Override
    public int getCount() {
        return 50;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.gridview, null);


            if(i<length){
                final Button btn = convertView.findViewById(R.id.button);
                btn.setBackgroundColor(Color.RED);
            }
            else {
                final Button btn = convertView.findViewById(R.id.button);
                btn.setBackgroundColor(Color.GREEN);
            }

        }

           // final Button btn = convertView.findViewById(R.id.button);

        return convertView;
    }
}
