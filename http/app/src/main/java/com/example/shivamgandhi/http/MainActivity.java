package com.example.shivamgandhi.http;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {


    public static final String URLDATA="http://192.168.0.28/basic/bus_info.php";
    public static final String URLDATA1="http://192.168.0.28/basic/user_score.php";
    String ID = "id";
    String NAME = "book_name";
    String PRICE = "price";
    ListView listView;
    ArrayList<HashMap<String,String>> arrayList ;
    Button btn,prev_btn;

        @Override
        protected void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            btn = findViewById(R.id.next_page);
            prev_btn = findViewById(R.id.prev_page);
            arrayList = new ArrayList<>();
            listView = (ListView)findViewById(R.id.sqlServer_list);
            new Getdata().execute();
            new GetAnotherData().execute();
            prev_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ii = new Intent(MainActivity.this,splashActivity.class);
                    startActivity(ii);
                }
            });
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(MainActivity.this,loginActivity.class);
                    startActivity(i);
                }
            });
        }
        class Getdata extends AsyncTask<Void,Void,Void>
        {
            ProgressDialog pd;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd= new ProgressDialog(MainActivity.this);
                pd.setTitle("Loading");
                pd.setMessage("Please Wait");
                pd.show();
            }

            @Override
            protected Void doInBackground(Void... params) {

                ServiceClass serviceClass = new ServiceClass();
                String result = serviceClass.getData(URLDATA);
                Log.e("wait what? :: " ,""+result);

                try {
                    if (result != null)
                    {
                        Log.e("error before array","dbh");
                        JSONArray jsonArray = new JSONArray(result);
                        Log.e("length of array",""+jsonArray.length());
                        for(int i = 0;i<jsonArray.length();i++){

                            Log.e("error after array","dbh");
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            //int id = jsonObject.getInt(ID);
                            String name = jsonObject.getString("student_id");
                            String price = jsonObject.getString("status");

                            Log.e("name and price ::",name+"\t"+price);
                            HashMap<String,String> hm = new HashMap<>();
                           // hm.put(ID,id+"");
                            hm.put(NAME,name);
                            hm.put(PRICE,price);
                            arrayList.add(hm);

                        }
                    }
                }
                catch (Exception e)
                {

                }
                return null;
            }
            @Override
            protected void onPostExecute(Void aVoid)
            {
                super.onPostExecute(aVoid);
                pd.dismiss();
                SimpleAdapter ad = new SimpleAdapter(MainActivity.this,arrayList,R.layout.custom_3,new String[]{NAME,PRICE},new int[]{R.id.textView2,R.id.textView3});
                listView.setAdapter(ad);
            }
        }



    class GetAnotherData extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd= new ProgressDialog(MainActivity.this);
            pd.setTitle("Loading");
            pd.setMessage("Please Wait");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceClass serviceClass = new ServiceClass();
            String result = serviceClass.getData(URLDATA1);
            Log.e("wait what? :: " ,""+result);

            try {
                if (result != null)
                {
                    //Log.e("error before array","dbh");
                    JSONArray jsonArray = new JSONArray(result);
                    //Log.e("length of array",""+jsonArray.length());
                    for(int i = 0;i<jsonArray.length();i++){

                       // Log.e("error after array","dbh");
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        //int id = jsonObject.getInt(ID);
                        String name = jsonObject.getString("user_info");


                        Log.e("second URL data ::",name);
                        Log.e("second URL data ::",name);
                        Log.e("second URL data ::",name);

                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            pd.dismiss();

        }
    }
    }

