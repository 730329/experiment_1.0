package com.example.shivamgandhi.http;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;

public class signupActivity extends AppCompatActivity {

    public static final String URLDATA="http://172.20.10.5/basic/bus_info.php";
    EditText student_info, confirm_password, emergencypin;
    EditText password;
    Button signup;
    String s_info;
    int score = 300;
    String pass, confirm_pass;
    int emergancyPin;
    TextView logIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        student_info = findViewById(R.id.signupActivity_student_id);
        password = findViewById(R.id.signupActivity_password);
        confirm_password = findViewById(R.id.signupActivity_confirm_password);
        emergencypin = findViewById(R.id.signupActivity_emergancypin);
        signup = findViewById(R.id.signupActivity_signup);
        logIn = findViewById(R.id.signupActivity_login);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                s_info = student_info.getText().toString();
                pass = password.getText().toString();
                confirm_pass = confirm_password.getText().toString();
                emergancyPin = Integer.valueOf(emergencypin.getText().toString());


                if (s_info.isEmpty() || pass.isEmpty() || confirm_pass.isEmpty() || emergencypin.getText().toString().isEmpty()) {

                    Toast.makeText(signupActivity.this, "One or More blank field(s).", Toast.LENGTH_SHORT).show();

                } else {

                    new AsyncPostData().execute(s_info, pass, String.valueOf(emergancyPin));
                }
            }
        });

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(signupActivity.this,loginActivity.class);
                startActivity(intent);
            }
        });

    }

    private class AsyncPostData extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {


            String data = null;
            try {
                data = URLEncoder.encode("student_id", "UTF-8")
                        + "=" + URLEncoder.encode(params[0], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                data += "&" + URLEncoder.encode("password", "UTF-8") + "="
                        + URLEncoder.encode(params[1], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                data += "&" + URLEncoder.encode("security_number", "UTF-8")
                        + "=" + URLEncoder.encode("" + params[2], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            String text = "";
            BufferedReader reader = null;

            try {

                // Defined URL  where to send data
                URL url = new URL("http://172.20.10.5/basic/abc.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
                Log.d("RESP", text);
            } catch (Exception ex) {
                Log.d("ERRRRRRR", ex.toString());
            } finally {
                try {

                    reader.close();
                } catch (Exception ex) {
                }
            }
            return new String("Success");
        }


        @Override
        protected void onPostExecute(String resp) {
            Log.d("SUCCESS---SUCCESS", "SUCCESS");
            new assignScore().execute(s_info," "+score);
            Intent intent = new Intent(signupActivity.this,splashActivity.class);
            intent.putExtra("student_id", s_info);
            startActivity(intent);

        }

    }
    class assignScore extends AsyncTask<String,String,String>
    {

        @Override
        protected String doInBackground(String... params) {


            String data = null;
            try {
                data = URLEncoder.encode("user_info", "UTF-8")
                        + "=" + URLEncoder.encode(params[0], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                data += "&" + URLEncoder.encode("score", "UTF-8") + "="
                        + URLEncoder.encode(params[1], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            String text = "";
            BufferedReader reader = null;

            try {

                // Defined URL  where to send data
                URL url = new URL("http://172.20.10.5/basic/assignScore.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
                Log.d("RESP", text);
            } catch (Exception ex) {
                Log.d("ERRRRRRR", ex.toString());
            } finally {
                try {

                    reader.close();
                } catch (Exception ex) {
                }
            }
            return new String("Success");
    }

}}
