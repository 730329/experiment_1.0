package com.example.shivamgandhi.http;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class profileActivity extends AppCompatActivity {

    public static final String URLDATA="http://172.20.10.5/basic/user_score.php";
    String student_info;
    String score;
    boolean b =false;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        tv = findViewById(R.id.yourScore);
        student_info = getIntent().getExtras().getString("user_name");
        new Getdata().execute();

    }
    class Getdata extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd= new ProgressDialog(profileActivity.this);
            pd.setTitle("Loading");
            pd.setMessage("Please Wait");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceClass serviceClass = new ServiceClass();
            String result = serviceClass.getData(URLDATA);
           // Log.e("wait what? :: " ,""+result);

            try {
                if (result != null)
                {
                   // Log.e("error before array","dbh");
                    JSONArray jsonArray = new JSONArray(result);
                   // Log.e("length of array",""+jsonArray.length());
                    for(int i = 0;i<jsonArray.length();i++){

                       // Log.e("error after array","dbh");
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        //int id = jsonObject.getInt(ID);
                        String name = jsonObject.getString("user_info");
                        String price = jsonObject.getString("score");

                        if(name.equals(student_info))
                        {
                            b = true;
                            score = price;

                        }

                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            pd.dismiss();
            if(b){
                Log.e("score",score);
                tv.setText("Your Score is:- "+score);

            }

        }

}}
