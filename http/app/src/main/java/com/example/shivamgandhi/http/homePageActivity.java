package com.example.shivamgandhi.http;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;

public class homePageActivity extends AppCompatActivity {

    String student_info,diff;
    int length  ;
    String time;
    public static final String URLDATA="http://172.20.10.5/basic/bus_info.php";
    String URLDATA3="http://worldclockapi.com/api/json/est/now";
    public static final String URLDATA1="http://172.20.10.5/basic/bus_info.php";
    public static final String URLDATA2="http://172.20.10.5/basic/bus_info.php";
    public static final String URLDATA4="http://172.20.10.5/basic/user_score.php";
    Button book,cancel,timer,profile;
    boolean bookBtn = false;
    boolean checkpoint = false;
    String Score;
    boolean scoreCheck = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        student_info = getIntent().getExtras().getString("student_id");
        diff = getIntent().getExtras().getString("diff");

        book=findViewById(R.id.bookMySeat);
        cancel=findViewById(R.id.cancelMySeat);
        timer=findViewById(R.id.timer_home);
        profile=findViewById(R.id.profile);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(homePageActivity.this,profileActivity.class);
                intent.putExtra("user_name",student_info);
                startActivity(intent);
            }
        });


       // Toast.makeText(this, "student:- "+student_info+" diff:- "+diff, Toast.LENGTH_SHORT).show();
        new homePageActivity.GetTime().execute();
       // new homePageActivity.getScore().execute();

        if(checkpoint){
            Toast.makeText(this, "Nothing to display", Toast.LENGTH_SHORT).show();
        }
        else{
     //  new homePageActivity.Getdata().execute();
      // new homePageActivity.decideButton().execute();


        book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //new homePageActivity.bookSeat().execute();
                Toast.makeText(homePageActivity.this, "length - "+length, Toast.LENGTH_SHORT).show();
                int remaining_seat = 50 - length;
                int waiting = 10;
                String status = "going";
                if(remaining_seat > 0){

                }
                else {

                    waiting = waiting - 1;

                }
                new homePageActivity.bookSeat().execute(student_info,status,  String.valueOf(remaining_seat),String.valueOf(waiting));

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new homePageActivity.cancelSeat().execute(student_info);
                new homePageActivity.getScore().execute();
            //    Toast.makeText(homePageActivity.this, "Testing Score: "+Score, Toast.LENGTH_SHORT).show();
                //new homePageActivity.updateScore().execute(student_info,Score);
            }
        });}
    }
    class Getdata extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd= new ProgressDialog(homePageActivity.this);
            pd.setTitle("Loading");
            pd.setMessage("Please Wait");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            length = 0;
            ServiceClass serviceClass = new ServiceClass();
            String result = serviceClass.getData(URLDATA);
            Log.e("wait what? :: " ,""+result);

            try {
                if (result != null)
                {

                    JSONArray jsonArray = new JSONArray(result);

                    for(int i = 0;i<jsonArray.length();i++){


                        JSONObject jsonObject = jsonArray.getJSONObject(i);


                        String status = jsonObject.getString("status");

                        if(status.equals("going")){
                            Log.e("inside","inside");
                            length += 1;

                        }



                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            pd.dismiss();
            GridView gridView = (GridView)findViewById(R.id.gridview);
            Log.e("sending Length",""+length);
            gridAdapter booksAdapter = new gridAdapter(homePageActivity.this, length);
            gridView.setAdapter(booksAdapter);
        }
    }

    ////

    class bookSeat extends AsyncTask<String,String,String>
    {
        @Override
        protected String doInBackground(String... params) {


            String data = null;
            try {
                data = URLEncoder.encode("student_id", "UTF-8")
                        + "=" + URLEncoder.encode(params[0], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                data += "&" + URLEncoder.encode("status", "UTF-8") + "="
                        + URLEncoder.encode(params[1], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                data += "&" + URLEncoder.encode("seats_remaining", "UTF-8")
                        + "=" + URLEncoder.encode("" + params[2], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                data += "&" + URLEncoder.encode("waiting", "UTF-8") + "="
                        + URLEncoder.encode(params[3], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            String text = "";
            BufferedReader reader = null;

            try {

                // Defined URL  where to send data
                URL url = new URL("http://172.20.10.5/basic/bookSeat.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
                Log.d("RESP", text);
            } catch (Exception ex) {
                Log.d("ERRRRRRR", ex.toString());
            } finally {
                try {

                    reader.close();
                } catch (Exception ex) {
                }
            }
            return new String("Success");
        }


        @Override
        protected void onPostExecute(String resp) {
            Log.d("SUCCESS---SUCCESS", "SUCCESS");
            new homePageActivity.Getdata().execute();
            Toast.makeText(homePageActivity.this, "Seat Reserved", Toast.LENGTH_SHORT).show();
            homePageActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    book.setVisibility(View.INVISIBLE);
                    cancel.setVisibility(View.VISIBLE);
                }
            });

        }

    }
/////

    class cancelSeat extends AsyncTask<String,String,String>
    {
        @Override
    protected String doInBackground(String... params) {


        String data = null;
        try {
            data = URLEncoder.encode("student_id", "UTF-8")
                    + "=" + URLEncoder.encode(params[0], "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String text = "";
        BufferedReader reader = null;

        try {

            // Defined URL  where to send data
            URL url = new URL("http://172.20.10.5/basic/cancelSeat.php");

            // Send POST data request

            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();


            // Get the server response

            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = null;

            // Read Server Response
            while ((line = reader.readLine()) != null) {
                // Append server response in string
                sb.append(line + "\n");
            }


            text = sb.toString();
            Log.d("RESP", text);
        } catch (Exception ex) {
            Log.d("ERRRRRRR", ex.toString());
        } finally {
            try {

                reader.close();
            } catch (Exception ex) {
            }
        }
        return new String("Success");
    }


        @Override
        protected void onPostExecute(String resp) {
            Log.d("SUCCESS---SUCCESS", "SUCCESS");
            new homePageActivity.Getdata().execute();
            Toast.makeText(homePageActivity.this, "Seat Cancelled", Toast.LENGTH_SHORT).show();
            homePageActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    book.setVisibility(View.VISIBLE);
                    cancel.setVisibility(View.INVISIBLE);

                    Log.d("Score",Score);
                    //int updatedScore = Integer.parseInt(Score)/2;
                    //Log.d("updatedScore",""+updatedScore);
                    //new updateScore().execute(student_info,""+updatedScore);

                }
            });

        }
    }

    ////

    class decideButton extends AsyncTask<Void,Void,Void>
    {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd= new ProgressDialog(homePageActivity.this);
            pd.setTitle("Loading");
            pd.setMessage("Please Wait");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceClass serviceClass = new ServiceClass();
            String result = serviceClass.getData(URLDATA);
            Log.e("wait what? :: " ,""+result);

            try {
                if (result != null)
                {

                    JSONArray jsonArray = new JSONArray(result);

                    for(int i = 0;i<jsonArray.length();i++){


                        JSONObject jsonObject = jsonArray.getJSONObject(i);


                        String status = jsonObject.getString("student_id");

                        if(status.equals(student_info)){
                            Log.e("inside","inside");
                            bookBtn = true;

                        }



                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            pd.dismiss();
            GridView gridView = (GridView)findViewById(R.id.gridview);
            Log.e("sending Length",""+length);
            if(bookBtn) {
                homePageActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        book.setVisibility(View.INVISIBLE);
                        cancel.setVisibility(View.VISIBLE);
                    }
                });
            }
        }
    }




    ////



    class GetTime extends AsyncTask<Void,Void,Void> {
    ProgressDialog pd;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd = new ProgressDialog(homePageActivity.this);
        pd.setTitle("Loading");
        pd.setMessage("Please Wait");
        pd.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        final String name;
        ServiceClass serviceClass = new ServiceClass();
        String result = serviceClass.getData(URLDATA3);
        Log.e("wait what? :: ", "" + result);

        try {
            if (result != null) {

                JSONObject jsonObject = new JSONObject(result);
                name = jsonObject.getString("currentDateTime");

                Log.e("Time and day ::", name );

                String []datetime = name.split("T");
                  time = datetime[1].substring(0,5);
                Log.d("TIME : ", time);
                homePageActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(splashActivity.this, ""+name, Toast.LENGTH_SHORT).show();
                       // timer.setText(time);
                        String []datetime = name.split("T");
                        time = datetime[1].substring(0,5);
                        String time11 = "10:30:00";
                        String time2 = time+":00";
                        Log.e("time2",time2);
                        Log.e("time2",time2);
                        Log.e("time2",time2);
                        Log.e("time2",time2);
                        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                        Date date1 = null;
                        try {
                            date1 = format.parse(time11);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Date date2 = null;
                        try {
                            date2 = format.parse(time2);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long difference = date1.getTime() - date2.getTime();
                        //long difference = date2.getTime() - date1.getTime();
                        Log.e("diff",""+difference);
        if (difference>7200000){
           checkpoint = true;

        }
        else {
            new CountDownTimer(difference, 1000) {

                public void onTick(long millisUntilFinished) {
                    timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    timer.setText("done!");
                    checkpoint = true;

                }

            }.start();
        }
                    }
                });


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        pd.dismiss();
        if(checkpoint){


            homePageActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(homePageActivity.this, "insode", Toast.LENGTH_SHORT).show();
                    book.setVisibility(View.INVISIBLE);
                    cancel.setVisibility(View.INVISIBLE);
                    profile.setVisibility(View.INVISIBLE);
                    timer.setVisibility(View.INVISIBLE);

                    Toast.makeText(homePageActivity.this, "Nothing To Display", Toast.LENGTH_SHORT).show();
                }
            });

        }
        else {

            new homePageActivity.Getdata().execute();
            new homePageActivity.decideButton().execute();

        }

        //String temp_val = "23:00:00";



    }
    }

    ///

    class getScore extends AsyncTask<Void,Void,Void> {
        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd= new ProgressDialog(homePageActivity.this);
            pd.setTitle("Loading");
            pd.setMessage("Please Wait");

            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceClass serviceClass = new ServiceClass();
            String result = serviceClass.getData(URLDATA4);
            Log.e("wait what? :: " ,""+result);

            try {
                if (result != null)
                {
                    //Log.e("error before array","dbh");
                    JSONArray jsonArray = new JSONArray(result);
                    //Log.e("length of array",""+jsonArray.length());
                    for(int i = 0;i<jsonArray.length();i++){

                      //  Log.e("error after array","dbh");
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        //int id = jsonObject.getInt(ID);
                        String name = jsonObject.getString("user_info");
                        String price = jsonObject.getString("score");



                       if(name.equals(student_info)){

                           Score = price;
                           scoreCheck = true;



                       }

                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            pd.dismiss();
            homePageActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new homePageActivity.updateScore().execute(student_info,Score);
                }
            });

        }}


    class updateScore extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(final String... params) {


            String data = null;
            try {
                data = URLEncoder.encode("user_info", "UTF-8")
                        + "=" + URLEncoder.encode(params[0], "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int y = Integer.parseInt(params[1]);
            homePageActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(homePageActivity.this, "Score went inside update score is "+params[1], Toast.LENGTH_SHORT).show();

                }
            });


            y = y/2;
            try {
                data += "&" + URLEncoder.encode("score", "UTF-8") + "="
                        + URLEncoder.encode(String.valueOf(y), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }


            String text = "";
            BufferedReader reader = null;

            try {

                // Defined URL  where to send data
                URL url = new URL("http://172.20.10.5/basic/updateScore.php");

                // Send POST data request

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();

                // Get the server response

                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "\n");
                }


                text = sb.toString();
                Log.d("RESP", text);
            } catch (Exception ex) {
                Log.d("ERRRRRRR", ex.toString());
            } finally {
                try {

                    reader.close();
                } catch (Exception ex) {
                }
            }
            return new String("Success");
        }
    }




    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


}
