package com.example.shivamgandhi.http;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class splashActivity extends AppCompatActivity {


    TextView tv1,tv2;
     String name;
    String URLDATA="http://worldclockapi.com/api/json/est/now";
    String student_info;

    CountDownTimer mCountDownTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);

        student_info = getIntent().getExtras().getString("student_id");

        tv1=findViewById(R.id.tp);
        tv2=findViewById(R.id.timer);

        new splashActivity.Getdata().execute();
    }

    class Getdata extends AsyncTask<Void,Void,Void> {
        ProgressDialog pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(splashActivity.this);
            pd.setTitle("Loading");
            pd.setMessage("Please Wait");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceClass serviceClass = new ServiceClass();
            String result = serviceClass.getData(URLDATA);
            Log.e("wait what? :: ", "" + result);

            try {
                if (result != null) {

                     JSONObject jsonObject = new JSONObject(result);
                     name = jsonObject.getString("currentDateTime");

                        Log.e("Time and day ::", name );

                    String []datetime = name.split("T");
                    final String time = datetime[1].substring(0,5);
                        Log.d("TIME : ", time);
                    splashActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(splashActivity.this, ""+name, Toast.LENGTH_SHORT).show();
                            tv1.setText(time);
                        }
                    });


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();


            String []datetime = name.split("T");
            final String time1 = datetime[1].substring(0,5);
            //String temp_val = "23:00:00";

            String time11 = "23:30:00";
            String time2 = "22:30:00";

            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
            Date date1 = null;
            try {
                date1 = format.parse(time11);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date date2 = null;
            try {
                date2 = format.parse(time2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long difference = date2.getTime() - date1.getTime();
            //long difference = date2.getTime() - date1.getTime();


            new CountDownTimer(difference, 1000) {

                public void onTick(long millisUntilFinished) {
                    tv2.setText("seconds remaining: " + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    tv2.setText("done!");
                }

            }.start();
            Intent i3 = new Intent(splashActivity.this,homePageActivity.class);
            i3.putExtra("diff",difference);
            i3.putExtra("student_id",student_info);
            startActivity(i3);
        }
    }


}
